const TailWind = require('tailwindcss');
const merge = require('webpack-merge');
const config = require('./webpack.config');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssNano = require('cssnano');
const PurgeCss = require('@fullhuman/postcss-purgecss');

module.exports = merge(config, {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [
                TailWind,
                PurgeCss({
                  content: ['./src/**/*.jsx', './src/**/*.js'],
                  defaultExtractor: (content) =>
                    content.match(/[A-Za-z0-9-_:/]+/g) || [],
                }),
                CssNano({
                  preset: 'default',
                }),
              ],
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
      chunkFilename: '[id].[hash].css',
    }),
  ],

  optimization: {
    chunkIds: 'named',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          priority: 10,
          enforce: true,
        },
      },
    },
  },
});
