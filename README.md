Webpack, React, Storybook, Taiwindcss
==

> Lesson 1: Setup project for React with Webpack, Storybook and Tailwind CSS.

### Install

```bash
$ git clone && yarn
```

### Tasks

- `start` - webpack dev server
- `storybook` - launch storybook ui
- `build` - build production version

### Setup Webpack

install webpack: https://webpack.js.org/guides/getting-started/

### Babel

add babel to webpack: https://babeljs.io/setup#installation
react preset: https://babeljs.io/docs/en/babel-preset-react

### Storybook

storybook for react: https://storybook.js.org/docs/guides/guide-react/#manual-setup

### TailwindCss

add tailwind: https://tailwindcss.com/docs/installation



