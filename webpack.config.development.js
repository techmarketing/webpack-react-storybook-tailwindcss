const TailWind = require('tailwindcss');
const merge = require('webpack-merge');
const config = require('./webpack.config');

module.exports = merge(config, {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [TailWind],
            },
          },
        ],
      },
    ],
  },
});
