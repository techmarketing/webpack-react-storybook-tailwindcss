import React from 'react';

import Button from './Button';

export default {
  title: 'Components',
};

export const button = () => <Button>My Button</Button>;
export const buttonDisabled = () => <Button disabled>My Button</Button>;
