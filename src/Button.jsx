import React from 'react';
import cn from 'classnames';

const Button = ({ children, disabled, ...props }) => {
  return (
    <button
      {...props}
      className={cn('font-bold', 'rounded', 'px-4', 'py-3', {
        'bg-green-100 text-gray-400 cursor-not-allowed': disabled,
        'bg-green-500 hover:bg-green-600 text-white': !disabled,
      })}
    >
      {children}
    </button>
  );
};

export default Button;
