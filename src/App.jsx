import React from 'react';
import CardImage from '../images/Group Copy 14.jpg';

const App = () => (
  <div>
    <h1>Hello From React!</h1>

    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio eaque
      eveniet illo ipsam, iusto, omnis porro ratione repellendus tempora vero
      vitae voluptatum. Architecto autem eius, explicabo facilis fugiat nisi
      officiis.
    </p>

    <img src={CardImage} alt="" />
  </div>
);

export default App;
