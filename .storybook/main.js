const custom = require('../webpack.config.js');

module.exports = {
  stories: ['../src/**/*.story.js{,x}'],

  webpackFinal: (config) => {
    return {
      ...config,
      module: { ...config.module, rules: custom.module.rules },
    };
  },
};
